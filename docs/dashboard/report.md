---
id: report
title: راهنمای استفاده از پنل کاربری تحت وب نانوواچ
sidebar_label: گزارش‌ها   
---

پنل کاربری تحت وب نانوواچ دسترسی نامحدود در عین سادگی


## گزارش‌ها در پنل تحت وب

کاربر در این قسمت می‌تواند اطلاعاتی جامع درباره پرسنل خود را مشاهده کند و همچنین فر‌ایندهایی مانند محاسبه کارکرد پرسنل برای مسایل  مربوط به حقوق و دستمزد را محاسبه و پردازش کند.
که به صورت خلاصه به شرح زیر می‌باشند:

>**ترددهای پرسنل :** مشاهده تمامی ترددهای تمام پرسنل با قابلیت فیلتر کردن و دریافت خروجی اکسل
>
>**درخواست‌های پرسنل :** این قسمت شامل تمام درخواست‌های پرسنل می‌باشد، که علاوه بر قابلیت دریافت خروجی اکسل، قابلیت ویراش توسط صاحب کسب و کار، ادمین و ویرایشگر را نیز دارا می‌باشد.
>
>**کارکرد روزانه پرسنل :** این قسمت شامل کارکرد روزانه تمام پرسنل می‌باشد که قابلیت اعمال انواع فیلتر و دریافت خروجی اکسل را نیز دارا می‌باشد.  
>
>**محاسبه کارکرد :** در این قسمت کاربر با انتخاب دوره کاری می‌تواند محاسبه کارکرد یک، چند یا همه پرسنل را انجام دهد.
>
>**کارکرد دوره‌ای پرسنل :** کاربران در این مرحله پس از ثبت درخواست محاسبه کارکرد در مرحله قبل می‌توانند خروجی اکسل کارکرد پرسنل خود را در دوره کاری که محاسبه کارکرد زده شده دریافت کنند.
>
>**گزارش سالیانه مرخصی‌ها :** کاربر در این قسمت می‌توانند به صورت خلاصه و در یک نگاه وضعیت مرخصی تمام پرسنل را مشاهده و تحلیل کند.  
>


![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/reports.png)


###     ترددهای پرسنل

در این قسمت تمامی ترددهای پرسنل قابل مشاهده هست و در صورت لزوم برای محاسبه اجباری زمان بین ۲ تردد به صورت کارکرد به طور مثال ۲۴ یا ۳۶ ساعت کارکرد برای یک پرسنل کافی است مطابق شکل زیر علامت **لینک سبز رنگ** ۲ تردد مد نظر خود را روشن کرده و با زدن **دکمه سبز لینک** می‌توانید این ۲ تردد را به هم متصل کرده و تمام زمان بینشان را به عنوان کارکرد برای پرسنل در نظر بگیرید.

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/traffic-link.png)

###     درخواست‌های پرسنل

در قسمت درخواست‌های پرسنل ادمین می‌تواند تمامی درخواست‌های پرسنل را ببیند یا با انتخاب کردن درخواست‌ها و فشار دادن دکمه **تغییر وضعیت درخواست‌ها** به صورت دستی درخواست‌های پرسنل را ویرایش کند.

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/request-admin.png)


###     ثبت درخواست گروهی 

با فشار دادن دکمه سبز رنگ **ثبت درخواست‌های گروهی** ادمین می‌تواند به صورت دستی برای پرسنل خود انواع درخواست که در قسمت **میز کار من** توضیح داده شده است را برای پرسنل مورد نظر خود ثبت کند.

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/add-request-admin.png)

####      نمونه درخواست ثبت مرخصی گروهی 

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/add-request-sample-admin.png)

### کارکرد روزانه پرسنل

این قسمت شامل کارکرد روزانه تمام پرسنل می‌باشد که قابلیت اعمال انواع فیلتر و دریافت خروجی اکسل را نیز دارا می‌باشد. همچنین در صورت نیاز میتوان مطابق تصویر زیر برای یک روز خاص مانند حالت قبل ادمین به صورت دستی برای پرسنل خود انواع درخواست را ثبت کند.

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/karkard-rozaneh.png)

###  محاسبه کارکرد دوره‌ای 

در این قسمت کاربر با انتخاب دوره کاری می‌تواند محاسبه کارکرد یک، چند یا همه پرسنل را انجام دهد.

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/karkard-doree.png)

###   کارکرد دوره‌ای پرسنل

کاربران در این مرحله پس از ثبت درخواست محاسبه کارکرد در مرحله قبل می‌توانند خروجی اکسل کارکرد پرسنل خود را در دوره کاری که محاسبه کارکرد زده شده دریافت کنند.

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/didan-karkard-doree.png)

###     گزارش سالیانه مرخصی‌ها

کاربر در این قسمت می‌توانند به صورت خلاصه و در یک نگاه وضعیت مرخصی تمام پرسنل را مشاهده و تحلیل کندهمچنین می‌توانید مقدار مانده مرخصی از سال قبل را برای انتقال به سال جدید مشخص کنید.

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/morakhasi-salaneh.png)

###     راهنمای دریافت خروجی اکسل از پنل تحت وب نانوواچ  

در بعضی از قسمت‌های گزارش‌ها در نانوواچ تعدادی دکمه در گوشه سمت چپ بالا مطابق عکس زیر قرار دارد که ممکن است تعدادشان در بعضی از صفحات کمتر باشند، از این دکمه‌ها برای دریافت خروجی اکسل بهینه شده از پنل حضور و غیاب نانوواچ استفاده می‌شود.

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/dokmeha.png)

###     دکمه سبز یا فیلترها 

با استفاده از این گزینه می‌توانید اطلاعات را بر اساس تاریخ، گروه‌های کاری، واحدهای کاری و یا یک یا چند پرسنل خاص ویرایش کنید.

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/sabz.png)

###     دکمه آبی یا فیلدها

بعد از فیلتر گزارشات در مرحله قبلی در این قسمت می‌توانید مقادیری که نیاز هست در خروجی فایل اکسل موجود باشد را انتخاب کرده و در نهایت قالب زمانی مورد نظر خود را انتخاب کنید و یک خروجی اکسل دریافت کنید.

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/abi.png)

###     دکمه بنفش یا آرشیو

این گزینه فقط در صفحه کارکرد روزانه در دسترس است و با انتخاب این گزینه و مشخص کردن تاریخ مد نظر در فرم مطابق عکس زیر می‌توانید تمامی اطلاعات را تا آن تاریخ آرشیو کرده تا دیگر قابل ویرایش یا تغییر نباشند.

![گزارش ها در نرم افزار حضور و غیاب](/img/V2/dashboard/banafsh.png)

