/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

class HomeSplash extends React.Component {
  render() {
    const {siteConfig, language = ''} = this.props;
    const {baseUrl, docsUrl} = siteConfig;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

    const SplashContainer = props => (
      <div className="homeContainer">
        <div className="homeSplashFade">
          <div className="wrapper homeWrapper">{props.children}</div>
        </div>
      </div>
    );

    const Logo = props => (
      <div className="projectLogo">
        <img src={props.img_src} alt="Project Logo" />
      </div>
    );

    const ProjectTitle = () => (
      <h2 className="projectTitle">
        {siteConfig.title}
        <small>{siteConfig.tagline}</small>
      </h2>
    );

    const PromoSection = props => (
      <div className="section promoSection">
        <div className="promoRow">
          <div className="pluginRowBlock">{props.children}</div>
        </div>
      </div>
    );

    const Button = props => (
      <div className="pluginWrapper buttonWrapper">
        <a className="button btn btn-unite btn-unite-default rounded-10" href={props.href} target={props.target}>
          {props.children}
        </a>
      </div>
    );

    return (
      <SplashContainer>
        <Logo img_src={`${baseUrl}img/loptop.svg`} />
        <div className="inner">
          <ProjectTitle siteConfig={siteConfig} />
          <PromoSection>
            <Button href={docUrl('device/device.html')}>راهنمای دستگاه حضور و غیاب</Button>
            <Button href={docUrl('app/intro.html')}>راهنمای اپلیکیشن موبایل</Button>
            <Button href={docUrl('dashboard/intro.html')}> راهنمای پنل تحت وب</Button>
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

class Index extends React.Component {
  render() {
    const {config: siteConfig, language = ''} = this.props;
    const {baseUrl} = siteConfig;

    const Block = props => (
      <Container
        padding={['bottom', 'top']}
        id={props.id}
        background={props.background}>
        <GridBlock
          align="center"
          contents={props.children}
          layout={props.layout}
        />
      </Container>
    );

    const FeatureCallout = () => (
      <div
        className="productShowcaseSection paddingBottom"
        style={{textAlign: 'center'}}>
        <h2>ویژگی‌های خاص نانوواچ</h2>
        <MarkdownBlock>ویژگی‌هایی که هیچ‌جا پیدا نمیشه</MarkdownBlock>
      </div>
    );

    const TryOut = () => (
      <Block id="try">
        {[
          {
            content:
              'نرم‌افزار نانوواچ  با اتکا به هوش مصنوعی خود همواره قابلیت ایجاد انواع شیفت مانند: چرخشی، شناور، ساعتی، اجباری و غیر اجباری برای هر پرسنل به تفکیک یا همه پرسنل را دارا می‌باشد که فرآیندهای محاسبه کارکرد پرسنل را از قبل ساده‌تر کرده و باعث می‌شود حجم زیادی از مسئولیت‌های بخش منابع سازمانی را خود به تنهایی انجام داده و بخش منابع انسانی می‌تواند در جهت رشد کسب و کار با خیالی راحت قدم بردارد.',
            image: `${baseUrl}img/hr.svg`,
            imageAlign: 'left',
            title: 'دسترسی‌های شگفت انگیز',
          },
        ]}
      </Block>
    );

    const Description = () => (
      <Block background="dark">
        {[
          {
            content:
              'شما با مطالعه این راهنما می‌توانید همواره از تمام قابلیت‌های کاربردی نانوواچ که  باعث رشد و تعالی سازمان شما میگردد مطلع شوید. **امیدواریم موفق باشید.**',
            image: `${baseUrl}img/app.svg`,
            imageAlign: 'right',
            title: 'توضیحات بیشتر',
          },
        ]}
      </Block>
    );

    const LearnHow = () => (
      <Block background="light">
        {[
          {
            content:
              'کسب و کارهایی که دارای چندین شعبه یا نمایندگی هستند به راحتی می‌توانند توسط سیستم پشتیبانی چندشاخه‌ای نانوواچ مدیریت شوند و نیاز به پیگیری مجزا در هر شعبه یا شاخه را ندارد، همچنین توسط این سیستم یک پرسنل می‌تواند به طور همزمان در یک یا چندین کسب و کار عضویت داشته و در هر کدام به طور مستقل انواع فرآیندهای پرسنلی خود را انجام دهد.',
            image: `${baseUrl}img/multi.svg`,
            imageAlign: 'right',
            title: 'دسترسی چندگانه پویا',
          },
        ]}
      </Block>
    );

    const Features = () => (
      <Block layout="fourColumn">
        {[
          {
            content: 'پشتیبانی ابری و دسترسی لحظه‌ای به اطلاعات',
            image: `${baseUrl}img/cloud.svg`,
            imageAlign: 'top',
            title: 'پایه ابری',
          },
          {
            content: 'ایجاد بی‌نهایت شیفت، تقویم و گروه کاری',
            image: `${baseUrl}img/unlimited.svg`,
            imageAlign: 'top',
            title: 'دسترسی بی‌نهایت',
          },
        ]}
      </Block>
    );

    const Showcase = () => {
      if ((siteConfig.users || []).length === 0) {
        return null;
      }

      const showcase = siteConfig.users
        .filter(user => user.pinned)
        .map(user => (
          <a href={user.infoLink} key={user.infoLink}>
            <img src={user.image} alt={user.caption} title={user.caption} />
          </a>
        ));

      const pageUrl = page => baseUrl + (language ? `${language}/` : '') + page;

      return (
        <div className="productShowcaseSection paddingBottom">
          <h2>از این‌جا شروع کنید</h2>
          <p>با کلیک بر روی دکمه زیر وارد مسیر یادگیری شوید.</p>
          <div className="logos">{showcase}</div>
          <div className="more-users">
            <a className="button btn btn-unite btn-unite-default rounded-10" href={pageUrl('docs/dashboard/intro.html')}>{siteConfig.title}
            </a>
          </div>
        </div>
      );
    };

    return (
      <div>
        <HomeSplash siteConfig={siteConfig} language={language} />
        <div className="mainContainer">
          <Features />
          <FeatureCallout />
          <LearnHow />
          <TryOut />
          <Description />
          <Showcase />
        </div>
      </div>
    );
  }
}

module.exports = Index;
