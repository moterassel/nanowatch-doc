/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary');

const Container = CompLibrary.Container;

const CWD = process.cwd();

const versions = require(`${CWD}/versions.json`);

function Versions(props) {
  const {config: siteConfig} = props;
  const latestVersion = versions[0];
  const repoUrl = `https://github.com/${siteConfig.organizationName}/${siteConfig.projectName}`;
  return (
    <div className="docMainWrapper wrapper">
      <Container className="mainContainer versionsContainer">
        <div className="post">
          <header className="postHeader">
            <h1>نسخه‌های {siteConfig.title} </h1>
          </header>
          <p>نسخه های جدید این پروژه هر چند وقت یکبار منتشر می شود.</p>
          <h3 id="latest">نسخه رسمی</h3>
          <table className="versions">
            <tbody>
              <tr>
                <th>{latestVersion}</th>
                <td>
                  {/* You are supposed to changes this href where appropriate
                        Example: href="<baseUrl>/docs(/:language)/:id" */}
                  <a
                    href={`${siteConfig.baseUrl}${siteConfig.docsUrl}/${
                      props.language ? props.language + '/' : ''
                    }dashboard/intro`}>
                    مشاهده
                  </a>
                </td>
                <td>
                  <a href="/blog/1400/03/13/version-2-2-7/">تغییرات این نسخه </a>
                </td>
              </tr>
            </tbody>
          </table>
          <p>
            این راهنما آموزش نسخه پایدار نانوواچ برای استفاده عموم می‌باشد.
          </p>
    {/*
          <h3 id="rc">نسخه غیر رسمی نانوواچ</h3>
          <table className="versions">
            <tbody>
              <tr>
                <th>نامشخص</th>
                <td>
                  You are supposed to changes this href where appropriate
                        Example: href="<baseUrl>/docs(/:language)/next/:id"
                  <a
                    href={`${siteConfig.baseUrl}${siteConfig.docsUrl}/${
                      props.language ? props.language + '/' : ''
                    }next/dashboard/intro`}>
                    مشاهده
                  </a>
                </td>
                <td>
                  <a href={repoUrl}>  تغییرات این نسخه</a>
                </td>
              </tr>
            </tbody>
          </table>
          <p>این نسخه جهت استفاده تست بود و هیچ تضمین عملکردی ندارد.</p>
    */}
          <h3 id="archive">نسخه‌های قدیمی</h3>
          <p>مشاهده راهنماهای نسخه‌های قدیمی</p>
        <a href="https://www.instagram.com/nanowatch_org/channel/">ویدیوهای آموزشی نانوواچ  </a>
          <table className="versions">
            <tbody>
              {versions.map(
                version =>
                  version !== latestVersion && (
                    <tr>
                      <th>{version}</th>
                      <td>
                        {/* You are supposed to changes this href where appropriate
                        Example: href="<baseUrl>/docs(/:language)/:version/:id" */}
                        <a
                          href={`${siteConfig.baseUrl}${siteConfig.docsUrl}/${
                            props.language ? props.language + '/' : ''
                          }${version}/dashboard/intro`}>
                          مشاهده
                        </a>
                      </td>
                      <td>
                        <a href="https://doc.nanowatch.org/blog">
                          تغییرات این نسخه
                        </a>
                      </td>
                    </tr>
                  ),
              )}
            </tbody>
          </table>

    {/*           <p>
            You can find past versions of this project on{' '}
            <a href={repoUrl}>GitHub</a>.
          </p> */}

        </div>
      </Container>
    </div>
  );
}

module.exports = Versions;
