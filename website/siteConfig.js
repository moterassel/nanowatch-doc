/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// See https://docusaurus.io/docs/site-config for all the possible
// site configuration options.

// List of projects/orgs using your project for the users page.
const users = [
  {
    caption: 'اولین قدم',
    // You will need to prepend the image path with your baseUrl
    // if it is not '/', like: '/test-site/img/image.jpg'.
    image: '/img/start.svg',
    infoLink: 'docs/app/register.html',
    pinned: true,
  },
];

const siteConfig = {
  gaTrackingId: 'UA-138714233-4',
  title: 'راهنمای جامع نانوواچ', // Title for your website.
  tagline: 'نظارت کمتر، نظم بیشتر',
  url: 'https://doc.nanowatch.org/', // Your website URL
  baseUrl: '/', // Base URL for your project */
  // For github.io type URLs, you would set the url and baseUrl like:
  //   url: 'https://facebook.github.io',
  //   baseUrl: '/test-site/',

  // Used for publishing and more
  defaultVersionShown: '2.2.7',
  projectName: 'NanoWatch Doc',
  organizationName: 'NanoWatch',
  onPageNav: 'separate',
  // For top-level user or org sites, the organization is still the same.
  // e.g., for the https://JoelMarcey.github.io site, it would be set like...
  //   organizationName: 'JoelMarcey'

  // For no header links in the top nav bar -> headerLinks: [],
  headerLinks: [
    {href: 'https://nanowatch.org', label: 'صفحه نخست نانوواچ'},
    {doc: 'dashboard/intro', label: ' راهنمای پنل تحت وب'},
    {doc: 'app/intro', label: ' راهنمای اپلیکیشن موبایل'},
    {doc: 'device/device', label: 'راهنمای دستگاه حضور غیاب'},
    {page: 'help', label: 'دانشنامه'},
    {href: 'https://blog.nanowatch.org', label: 'بلاگ'},
    {blog: true, label: 'تغییرات نانوواچ (آپدیت‌ها)'},
    // { search: true }
  ],

  // If you have users set above, you add it here:
  users,

  /* path to images for header/footer */
  headerIcon: 'img/logo-nanowatch.png',
  footerIcon: 'img/logo-nanowatch.png',
  favicon: 'img/favicon.png',

  /* Colors for website */
  colors: {
    primaryColor: '#4a148c',
    secondaryColor: '#aa00ff',
  },

  /* Custom fonts for website */
  /*
  fonts: {
    myFont: [
      "Times New Roman",
      "Serif"
    ],
    myOtherFont: [
      "-apple-system",
      "system-ui"
    ]
  },
  */
  // stylesheets: [
  //   '/css/app.css',
  // ],

  // This copyright info is used in /core/Footer.js and blog RSS/Atom feeds.
  copyright: ` © ${new Date().getFullYear()} تمامی حقوق برای نانوواچ محفوظ است. `,

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks.
    theme: 'default',
  },

  // Add custom scripts here that would be placed in <script> tags.
  scripts: ['https://buttons.github.io/buttons.js'],

  // On page navigation for the current documentation page.
  // No .html extensions for paths.
  cleanUrl: true,

  // Open Graph and Twitter card images.
  ogImage: 'img/undraw_online.svg',
  twitterImage: 'img/undraw_tweetstorm.svg',

  // For sites with a sizable amount of content, set collapsible to true.
  // Expand/collapse the links and subcategories under categories.
  docsSideNavCollapsible: true,

  // Show documentation's last contributor's name.
  // enableUpdateBy: true,

  // Show documentation's last update time.
  enableUpdateTime: true,

  // You may provide arbitrary config keys to be used as needed by your
  // template. For example, if you need your repo's URL...
  //   repoUrl: 'https://github.com/facebook/test-site',
  scrollToTop: true,
  scrollToTopOptions: {
    zIndex: 100,
  },
};

module.exports = siteConfig;
