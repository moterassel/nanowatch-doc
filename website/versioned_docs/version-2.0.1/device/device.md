---
id: version-2.0.1-device
title: راهنمای استفاده از دستگاه حضور و غیاب  نانوواچ
sidebar_label: راه‌اندازی دستگاه
original_id: device
---

راهنمای استفاده از [دستگاه حضور و غیاب  نانوواچ](https://nanowatch.org/%D9%86%D8%A7%D9%86%D9%88-%D9%88%D8%A7%DA%86-nano-watch/%D8%AF%D8%B3%D8%AA%DA%AF%D8%A7%D9%87-%D8%AD%D8%B6%D9%88%D8%B1-%D9%88-%D8%BA%DB%8C%D8%A7%D8%A8-%D9%86%D8%A7%D9%86%D9%88%D9%88%D8%A7%DA%86/) دقیق و هوشمند.

## پیش نیاز راه‌اندازی دستگاه 

در اولین قدم استفاده از دستگاه اثر انگشت نانوواچ لازم است پس از ثبت نام در سامانه حضور غیاب نانوواچ با مراجعه به پنل کاربری خود و انتخاب گزینه تنظیمات کسب و کار و سپس گزینه تنظیمات عمومی وارد صفحه‌ای مانند صفحه زیر می‌شوید، که در قدم‌های بعدی نیاز به وارد کردن کد مشخص شده در عکس را دارید تا دستگاه اثر انگشت خود را فعال و قابل استفاده کنید.

![پیش نیاز راه‌اندازی دستگاه](/img/device/code.png)

## پنل تنظیمات دستگاه 

در قدم بعدی با اتصال دستگاه اثر انگشت خود به برق و پیدا کردن سیگنال وای-فای دستگاه با نام NanoWatchAP با وارد کردن رمز عبور  و اتصال به آن می‌توانید با وارد کردن IP:172.24.1.1  در مرورگر خود و وارد کردن نام کاربری و رمز عبور وارد تنظیمات دستگاه شوید. و یک شرکت مطابق شکل زیر ایجاد کنید.

![پنل تنظیمات دستگاه](/img/device/register-org.png)

## اضافه کردن پرسنل

در قدم بعدی برای اضافه کردن پرسنل شرکت خود مطابق عکس زیر باید عمل کنید.

![اضافه کردن پرسنل](/img/device/add-personnel.png)

## ثبت پرسنل 

و در قدم بعدی باید تمامی اطلاعات پرسنلی پرسنل خود را مانند تصویر زیر وارد نمایید، و در انتها دکمه ذخیره را فشار دهید تا دستگاه آماده دریافت اثر انگشت پرسنل شما جهت ذخیره سازی شود. سپس در این مرحله پرسنل شما باید انگشت خود را بر روی سنسور اثر انگشت قرار دهد تا یک صدای بوق بشنود سپس انگشت خود را برداشته و دوباره مرحله فوق را تکرار کند تا اثر انگشت شخص در دستگاه ذخیره گردد.

![اضافه کردن پرسنل](/img/device/register-personnel.png)
