---
id: version-1.0.0-report
title: راهنمای استفاده از پنل کاربری تحت وب نانوواچ
sidebar_label: گزارش‌ها
original_id: report
---

پنل کاربری تحت وب نانوواچ دسترسی نامحدود در عین سادگی


## گزارش‌ها در پنل تحت وب

کاربر در این قسمت می‌تواند اطلاعاتی جامع درباره پرسنل خود را مشاهده کند و همچنین فر‌ایندهایی مانند محاسبه کارکرد پرسنل برای مسایل  مربوط به حقوق و دستمزد را محاسبه و پردازش کند.

![گزارش ها در نرم افزار حضور و غیاب](/img/dashboard/reports.png)
