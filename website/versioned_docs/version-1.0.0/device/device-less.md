---
id: version-1.0.0-device-less
title: راهنمای استفاده از دستگاه  بدون تماس نانوواچ
sidebar_label: راه‌اندازی دستگاه
original_id: device-less
---

راهنمای استفاده از [دستگاه بدون تماس، حضور و غیاب نانوواچ](https://nanowatch.org/%D9%86%D8%A7%D9%86%D9%88-%D9%88%D8%A7%DA%86-nano-watch/%D8%AF%D8%B3%D8%AA%DA%AF%D8%A7%D9%87-%D8%AD%D8%B6%D9%88%D8%B1-%D9%88-%D8%BA%DB%8C%D8%A7%D8%A8-%D9%86%D8%A7%D9%86%D9%88%D9%88%D8%A7%DA%86/) دقیق و هوشمند.

<div class="row">
<img class="col-8 col-md-5 mx-auto p-0" src="/img/device/device-less.png" alt="دستگاه تردد بدون تماس">
</div>

## افزودن دستگاه به کسب و کار

پس از نصب برنامه از مارکت های معتبر و ورود به حساب کاربری خود، علامت سه نقطه بالای اپلیکیشن را انتخاب نموده و وارد **تنظیمات کسب و کار** می‌شوید

[دانلود اپلیکیشن اندروید](https://play.google.com/store/apps/details?id=ca.nanowatch.app)

[دانلود اپلیکیشن آیفون](https://apps.apple.com/ca/app/nanowatch/id1474516519)

<div class="row">
<img class="col-8 col-md-3 mx-auto p-0" src="/img/device/install-app.png" alt="نصب اپلیکیشن نانو واچ">
<img class="col-8 col-md-3 mx-auto p-0" src="/img/device/main-app.jpg" alt="تنظیمات عمومی اپلیکیشن نانو واچ">
<img class="col-8 col-md-3 mx-auto p-0" src="/img/device/tenant-setting.jpg" alt="تنظیمات کسب و کار اپلیکیشن نانو واچ">
</div>

پس از وارد شدن به تنظیمات کسب و کار گزینه **روش‌های ثبت تردد** را انتخاب نمایید و سپس **تگ‌های ان اف سی (nfc)** را انتخاب نمایید. در این قسمت لیستی از تمامی ماژول های بدون تماس خود می‌بینید که کلیه دستگاه های خود را می‌توانید تنظیم کنید.

<div class="row">
<img class="col-8 col-md-3 mx-auto p-0" src="/img/device/select-connect-way.jpg" alt="راههای ثبت تردد در اپلیکیشن نانو واچ">
<img class="col-8 col-md-3 mx-auto p-0" src="/img/device/select-nfc-way.jpg" alt="انتخاب روش ماژول لدون تماس اپلیکیشن نانو واچ">
<img class="col-8 col-md-3 mx-auto p-0" src="/img/device/nfc-list.jpg" alt="لیست ماژول های بدون تماس در اپلیکیشن نانو واچ">
</div>

در این مرحله با انتخاب **دکمه مثبت پایین صفحه** می‌توانید یه ماژول را به اپلیکیشن متصل کنید، پس از انتخاب دکمه مثبت باید **گوشی خود را به ماژول نزدیک کنید** و سپس یک **عنوان برای این ماژول** انتخاب نموده و علامت تیک و تایید نهایی را انتخاب نمایید، هم اکنون دستگاه شما در کسب و کار ثبت شده است.

<div class="row">
<img class="col-8 col-md-3 mx-auto p-0" src="/img/device/add-nfc.jpg" alt="افزدون ماژول بدون تماس به اپلیکیشن نانو واچ">
<img class="col-8 col-md-3 mx-auto p-0" src="/img/device/connect-phone-to-nfc.jpg" alt="اتصال تلفن همراه به ماژول بدون تماس اپلیکیشن نانو واچ">
<img class="col-8 col-md-3 mx-auto p-0" src="/img/device/setup-nfc.jpg" alt="تنظیمات ماژول بدون تماس اپلیکیشن نانو واچ">
</div>

## انتصاب پرسنل مجاز

شما در این مرحله می توانید ماژولی که تعریف کرده‌اید را انتخاب نموده و **پرسنل مجاز** برای استفاده از این ماژول را انتخاب نمایید. شایان ذکر است تلفن همراه باید قابلیت اتصال بدون تماس از راه نزدیک را دارا باشد.

<div class="row">
<img class="col-8 col-md-3 mx-auto p-0" src="/img/device/nfc-users.jpg" alt="تنظیمات ماژول بدون تماس اپلیکیشن نانو واچ">
<img class="col-8 col-md-3 mx-auto p-0" src="/img/device/access-users.jpg" alt="دسترسی کاربران به ماژول بدون تماس اپلیکیشن نانو واچ">
</div>

[راهنمای ثبت تردد با ماژول بدون تماس توسط پرسنل](/docs/app/clockInOut#ثبت-تردد-با-ماژول-بدون-تماس)
